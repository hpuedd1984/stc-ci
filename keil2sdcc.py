#!/usr/bin/python3
# -*- coding:utf-8 -*-
import os
import re
import sys

#添加sdcc类型映射到 eddc51.h
def add_to_eddc51(rootpath):
    content = '''
    #ifndef __KEILTOSDCC_H_
    #define __KEILTOSDCC_H_
    #define idata __idata 
    #define data  __data 
    #define pdata __pdata 
    #define xdata __xdata
    #define code  __code
    #define bit   __bit
    #define using __using
    #define interrupt __interrupt
    #define _nop_() __asm NOP __endasm
    #endif
    '''
    tfh = open(os.path.normcase(rootpath+'/modules/eddc51.h'), 'r+', encoding='utf-8')
    sourcetext = tfh.read()
    if not content in sourcetext:
            tfh.seek(0,0)
            tfh.write(content+sourcetext)
    tfh.close()
    print('add sdcc type define to eddc51.h completely!')

#替换putchar getchar为 _putchar _getchar
def replace_stdio(rootpath):
    tfh = open(os.path.normcase(rootpath+'/bsp/board_V2_4/drivers/drv_uart.c'), 'r+', encoding='utf-8')
    text = tfh.read()
    text = text.replace(' putchar', ' _putchar')
    text = text.replace(' getchar', ' _getchar')
    tfh.seek(0,0)
    tfh.write(text)
    tfh.close()
    print('replace xutchar to _xutchar completely!')

#遍历（递归）目录下所有文件，找出.c .h文件
def find_all_file(rootpath):
    cfiles = []
    hfiles = []
    for fpathe, dirs, fs in os.walk(rootpath):
        for f in fs:
            ofp = os.path.join(fpathe,f)       
            if re.match('.*\.c$', ofp) is not None:
                cfiles.append(ofp)
            elif re.match('.*\.h$', ofp) is not None:
                hfiles.append(ofp)
    return cfiles, hfiles

def change_h_file(hfs):
    for f in hfs:
        fd = open(f, 'r+', encoding='utf-8')
        lines = fd.readlines()
        fd.seek(0,0)
        for line in lines:
            line = re.sub('(^\s*#include\s+<intrins.h>)', lambda m: r'//'+m.group(1), line)
            fd.write(line)    
        fd.close()
    print('head files modify completely!')
def change_c_file(cfs):
    for f in cfs:
        print(f)
        fd = open(f, 'r+', encoding='utf-8')
        try:
            lines = fd.readlines()
            tlist = []
            for line in lines:
                line = re.sub('sbit\s+(\S+)\s*=\s*(P\d)\^(\d)\s*;', lambda m: '#define '+m.group(1)+' '+m.group(2)+m.group(3), line)
                line = re.sub('(interrupt|using)\s+(\d)', lambda m: m.group(1)+' ('+m.group(2)+')', line)      
                line = re.sub('(^\s*#include\s+<intrins.h>)', lambda m: '//'+m.group(1), line)
                tlist.append(line)
            fd.seek(0,0)
            for line in tlist:
                fd.write(line)
            tailstr = '''/************************just a tet***********/
            '''
            for i in range(20):
                fd.write(tailstr)
            fd.close()
        except:
            print("error in %s!  break!" %f)
            sys.exit(0)
        else:
            print("%s is changed compeletly" %f)
    print('c files modify completely!')

def modify_chip_head_file(path):
    print('\nchang work path to: ', end='')
    os.chdir(path)
    print(os.getcwd())
    fname = 'stc12c5a60s2.h'
    if not os.path.isfile(fname):
        sys.exit('error: the specified file does not exist')
    fh = open(fname, encoding='utf-8')
    flines = fh.readlines()
    fh.seek(0, 0)
    fcontent = fh.read()
    fh.close()

    #获取 ^名称列表
    names = re.findall('(\S+)\^\d', fcontent)
    names = sorted(set(names), key=names.index)

    #获取^值, 构建字典
    dict = {}
    for i in names:
        format = 'sfr\s%s\s+=\s(0x..)' % i
        match = re.search(format, fcontent)
        dict[i] = match.group(1)  

    #替换 x^x 为 hex （addr）
    for i in names:    
        val = int(dict[i], 16)
        format = '%s\^(\d)' % i   
        cnt = 0 
        for j in flines:        
            j = re.sub(format, lambda m:hex(val+int(m.group(1))), j)        
            flines[cnt] = j
            cnt += 1

    #替换keil编译器的sfr,sbit 为SDCC形式  
    tfh = open(fname, 'w+', encoding='utf-8')
    for i in flines:
        i = re.sub('(sfr|sbit)\s(\S+)\s+=\s(.+);', lambda m:'__'+m.group(1)+' __at('+m.group(3)+') '+m.group(2)+';', i)
        tfh.write(i)     
    tfh.close()
    print('chip hader file modify completely!')
if __name__ == '__main__':
    rootpath = os.getcwd()
    print(rootpath)
    add_to_eddc51(rootpath)
    replace_stdio(rootpath)
    clist, hlist = find_all_file(rootpath)
    change_h_file(hlist)
    change_c_file(clist)    
    modify_chip_head_file('./bsp/board_V2_4')
    print('\nsucceed for keill2sdcc!\n')
